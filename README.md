# Multigraph Topology Design for Cross-Silo Federated Learning

This repository is the implementation of a decentralized federated learning approach. We benchmark our method on three public datasets: [Inaturalist](https://arxiv.org/abs/1707.06642), [FEMNIST](https://arxiv.org/abs/1812.01097), and [Sentiment-140](https://arxiv.org/abs/1812.01097).

This repository is based on and inspired by @Othmane Marfoq [work](https://github.com/omarfoq/communication-in-cross-silo-fl). We sincerely thank for their sharing of the codes.

### Prerequisites

PYTHON 3.6

CUDA 9.2

Please install dependence package by run following command:
```
pip install -r requirements.txt
```

### Datasets

* For FEMNIST dataset:

    * To download and preprocess them, please run:

        ```
        cd data/femnist
        bash download_preprocess.sh
        ```

    * To split the preprocessed dataset into silos for EXODUS network, please run:
 
        ```
        python3 split_data.py --num_workers 79
        ```
        With --num_workers: the number of workers (silos)
        
        | Network name | # Workers |
        |:------------:|:---------:|
        |     Gaia     |     11    |
        |    Amazon    |     22    |
        |     Géant    |     40    |
        |    Exodus    |     79    |
        |     Ebone    |     87    |
        
        We also provide the split FEMNIST dataset for EXODUS network at [link](https://vision.aioz.io/f/476da06ffe5b442eb6f6/?dl=1). You can download and extract them into "data/femnist/exodus" folder.
 * For iNaturalist dataset and Sentiment140 dataset:  please follow the instruction in [here](https://github.com/omarfoq/communication-in-cross-silo-fl)  
### Multigraph Networks and Topologies

Important: Before running any command lines in this section, please run following command to access 'graph_utils' folder:
```
cd graph_utils
```
And now, you are in 'graph_utils' folder.
* To generate baseline and multigraph networks for FEMNIST dataset and compute the cycle time for them:
    ```
    bash generate_network_femnist_table3.sh
    ```

* To generate baseline and multigraph networks for iNaturalist dataset and compute the cycle time for them:
    ```
    bash generate_network_inat_table3.sh
    ```

* To generate baseline and multigraph networks for Sentiment-140 dataset and compute the cycle time for them:
    ```
    bash generate_network_sent140_table3.sh
    ```
![Fig-1](misc/table_3.png)
*<center>**Table 1**: Cycle time (ms) comparison between different typologies. (↓◦) indicates our reduced times compared with other methods (Table 3 in the main paper).</center>*

### Training

To train our method on FEMNIST dataset with EXODUS network, run:

```
bash train_femnist_exodus_multigraph-ring.sh
```

#### Pretrained models
We provide the pretrained models which are trained on FEMNIST dataset with EXODUS network by our method at the last epoch. Please download at [link](https://vision.aioz.io/f/b7c0b1f25389457e85bf/?dl=1) and extracted them into the "pretrained_models/FEMNIST_EXODUS_MULTIGRAPH-RING" folder. The models can be evaluted in FEMNIST train and test set via:
```
bash eval_femnist_exodus_multigraph-ring.sh 
```

### Citation

If you use this code as part of any published research, we'd really appreciate it if you could cite the following paper:

```
Updating
```

### License

MIT License

